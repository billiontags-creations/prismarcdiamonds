/**
* Created by Kenzap on 21/10/2016.
*/

(function ($) {
  "use strict";
  var tfs_close = $(".tfs-close");
  var tfs_wrap = $(".tfs-wrap");
  var tfs_popup_bg = $(".tfs-popup-bg");
  var popup_search = $(".popup-search");
  var search_popup = $(".search-popup");
  var search_close = $(".search-close");
  var list_links = $(".list-links");
  var content_area = $("#content-area");
  var thumbnail_img = $(".thumb");

  if($(".scrollbar-inner").length)
    $(".scrollbar-inner").scrollbar();

  if($("#zoom").length)
    $("#zoom").elevateZoom();

//init menu
if($(".main-menu nav").length)
  $(".main-menu nav").meanmenu();

//init products slider
if($('.ex2').length)
  $('.ex2').slider({});

//init tooltip
if($(".tip").length)
  $(".tip").tooltip();

//limited time
if($("#time").length)
  $('#time').syotimer({
    year: $('#time').attr('data-year'),
    month: $('#time').attr('data-month'),
    day: $('#time').attr('data-day'),
    hour: $('#time').attr('data-hour'),
    minute: $('#time').attr('data-minute'),
  });

/*----------------------------
One Columns Slider
------------------------------ */
$(".columns1").owlCarousel({
  loop: true,
  autoPlay: true,
  dotsEach: true,
  nav: true,
  navText: ["<i class='flaticon flaticon-back'></i>", "<i class='flaticon flaticon-next'></i>"],
  items: 1,
  margin: 0,
  singleItem: true,
  autoplayTimeout: 5000
});

/*----------------------------
Two Columns Slider
------------------------------ */

$(".columns2").owlCarousel({
  loop: true,
  autoplay: true,
  responsiveClass: true,
  margin: 30,
  autoplayTimeout: 5000,
  responsive: {
    0: {
      items: 1,
      nav: true
    },
    480: {
      items: 1,
      nav: true
    },
    768: {
      items: 2,
      nav: true,
      loop: true
    },
    992: {
      items: 2,
      nav: true,
      loop: true
    }
  }
});

/*----------------------------
Three Columns Slider
------------------------------ */

$(".columns3").owlCarousel({
  loop: true,
  autoplay: true,
  nav: true,
  navText: ["<i class='flaticon flaticon-back'></i>", "<i class='flaticon flaticon-next'></i>"],
  responsiveClass: true,
  autoplayTimeout: 5000,
  margin: 30,
  size: 2,
  vertical: true,
  responsive: {
    0: {
      items: 1,
      nav: true
    },
    480: {
      items: 1,
      nav: true
    },
    768: {
      items: 2,
      nav: true,
      loop: true
    },
    992: {
      items: 3,
      nav: true,
      loop: true
    },
    1200: {
      items: 3,
      nav: true,
      loop: true
    }
  }
});
/*----------------------------
Three Columns Slider
------------------------------ */
$(".columns-star-pick").owlCarousel({
  loop: true,
  autoPlay: true,
  nav: true,
  navText: ["<i class='flaticon flaticon-back'></i>", "<i class='flaticon flaticon-next'></i>"],
  items: 1,
  margin: 0,
  singleItem: true,
  autoplayTimeout: 5000
});

/*----------------------------
Four Columns Slider
------------------------------ */

$(".columns4").owlCarousel({
  loop: true,
  autoplay: false,
  nav: true,
  navText: ["<i class='flaticon flaticon-back'></i>", "<i class='flaticon flaticon-next'></i>"],
  responsiveClass: true,
  margin: 30,
  autoplayTimeout: 5000,
  responsive: {
    0: {
      items: 1,
      nav: true
    },
    480: {
      items: 1,
      nav: true
    },
    768: {
      items: 2,
      nav: true,
      loop: true
    },
    992: {
      items: 3,
      nav: true,
      loop: true
    },
    1200: {
      items: 4,
      nav: true,
      loop: true
    }
  }
});

/*----------------------------
Four Columns Slider bestseller
------------------------------ */

$(".columns4-bestseller").owlCarousel({
  loop: true,
  autoplay: true,
  nav: true,
  navText: ["<i class='flaticon flaticon-back'></i>", "<i class='flaticon flaticon-next'></i>"],
  responsiveClass: true,
  margin: 31,
  autoplayTimeout: 5000,
  responsive: {
    0: {
      items: 1,
      nav: true
    },
    480: {
      items: 1,
      nav: true
    },
    768: {
      items: 2,
      nav: true,
      loop: true
    },
    992: {
      items: 3,
      nav: true,
      loop: true
    },
    1200: {
      items: 4,
      nav: true,
      loop: true
    }
  }
});

/*----------------------------
five Columns Slider
------------------------------ */
$(".columns5").owlCarousel({
  loop: true,
  autoplay: true,
  responsiveClass: true,
  margin: 30,
  autoplayTimeout: 5000,
  responsive: {
    0: {
      items: 1,
      nav: true
    },
    480: {
      items: 1,
      nav: true
    },
    768: {
      items: 3,
      nav: true,
      loop: true
    },
    992: {
      items: 4,
      nav: true,
      loop: true
    },
    1200: {
      items: 5,
      nav: true,
      loop: true
    }
  }
});
/*----------------------------
Six Columns Slider
------------------------------ */
$(".columns6").owlCarousel({
  loop: true,
  autoplay: true,
  responsiveClass: true,
  margin: 30,
  autoplayTimeout: 5000,
  responsive: {
    0: {
      items: 1,
      nav: true
    },
    480: {
      items: 1,
      nav: true
    },
    768: {
      items: 3,
      nav: true,
      loop: true
    },
    992: {
      items: 4,
      nav: true,
      loop: true
    },
    1200: {
      items: 6,
      nav: true,
      loop: true
    }
  }
});

/*----------------------------
gallery image thumb
------------------------------ */
$(".gallery-image-thumb").owlCarousel({
  loop: true,
  autoplay: true,
  responsiveClass: true,
  margin: 30,
  autoplayTimeout: 5000,
  responsive: {
    0: {
      items: 3,
      nav: true
    },
    480: {
      items: 3,
      nav: true
    }
  }
});

/*----------------------------
option product
------------------------------ */
var op_fil = $(".item-open");
var inner = $('.inner__filter');

/*----------------------------
option product mobile
------------------------------ */
var ex_mobile = $(".btn-expand");

/*----------------------------
sidebar
------------------------------ */
var collapse = $(".panel-title");
var off_collapse = $(".collapse-close");

/*----------------------------
Bind on click events
------------------------------ */
$("a,div,button,li,h5").on('click',function(e){

  if ( $(this).hasClass("tfs-close") ) {

    tfs_wrap.removeClass("tfs-popup-ready");
    tfs_popup_bg.removeClass("tfs-popup-ready");

  }else if ( $(this).hasClass("tfs-popup-bg") ) {

    tfs_wrap.removeClass("tfs-popup-ready");
    tfs_popup_bg.removeClass("tfs-popup-ready");
    popup_search.removeClass("search-show");

  }else if ( $(this).hasClass("search-popup") ) {

    popup_search.addClass("search-show");
    tfs_popup_bg.addClass("tfs-popup-ready");

  }else if ( $(this).hasClass("search-close") ) {

    popup_search.removeClass("search-show");
    tfs_popup_bg.removeClass("tfs-popup-ready");

  }else if ( $(this).hasClass("list-links") ) {

    var popup_content = $(this).siblings().html();
    content_area.html(popup_content);

  }else if ( $(this).hasClass("list-links") ) {

    var popup_content = $(this).siblings().html();
    content_area.html(popup_content);

  }else if ( $(this).hasClass("item-open") ) {

    $(this).toggleClass('active');
    $(this).siblings().removeClass('active');
    $(this).parent().siblings().children().removeClass('active');
    var i = $(this).children().attr('class');
    var x = inner.find("." + i);
    x.slideToggle();
    x.siblings().css("display", "none")

  }else if ( $(this).hasClass("btn-expand") ) {


    $(this).toggleClass('active');
    $(this).siblings(".block_panel").slideToggle();

  }else if ( $(this).hasClass("panel-title") ) {

    $(this).toggleClass('collapse-close');

  }else if ( $(this).hasClass("thumb") ) {

    var img_big = $(this).find(".btm-img").attr("data-image");
    $("#zoom").attr("src",img_big);
    // add active
    thumbnail_img.removeClass("active");
    $(this).addClass("active");
    $("#zoom").elevateZoom();

  }

});


})(jQuery);

function initMap() {
  var mapCanvas = document.getElementById("map");
  var myCenter = new google.maps.LatLng(51.508742, -0.120850);
  var mapOptions = {
    center: myCenter,
    zoom: 15,
    disableDoubleClickZoom: true,
    navigationControl: false,
    mapTypeControl: false,
    scaleControl: false,
    zoomControl: false,
    scrollwheel: false,
    styles: [
    {
      featureType: 'all',
      stylers: [
      { saturation: -80 }
      ]
    }, {
      featureType: 'road.arterial',
      elementType: 'geometry',
      stylers: [
      { hue: '#ccc' },
      { saturation: 50 }
      ]
    }, {
      featureType: 'poi.business',
      elementType: 'labels',
      stylers: [
      { visibility: 'off' }
      ]
    }
    ]
  };
  var map = new google.maps.Map(mapCanvas, mapOptions);
  var marker = new google.maps.Marker({
    position: myCenter,
  });
  var infowindow = new google.maps.InfoWindow({
    position: myCenter,
    content: "Klambi"
  });
  infowindow.open(map, marker);
}