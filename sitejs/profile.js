$(function () {
    profile();
    holdlist();
    enquirelist();
    orders(orders_api);
});

var page_no = 1;

function profile() {
    commonAjax(myprofile_api, 'listWithToken')
        .done(function (data) {
            console.log(data);
            $(".profilename").text(data.first_name + " " + data.last_name);
            $(".profilephone").text(data.username);
            $(".profileemail").text(data.email);
            if (data.userprofile.profile_pic) {
                $(".profilepic").attr("src", data.userprofile.profile_pic);
                $(".changeprofilepic").css("background-image", `url(${data.userprofile.profile_pic})`);
            } else {
                $(".profilepic").attr("src", "images/profiledummy.jpg");
                $(".changeprofilepic").css("background-image", `url(images/profiledummy.jpg)`);
            }
            $(".profileip").eq(0).val(data.userprofile.firm_name);
            $(".profileip").eq(1).val(data.first_name);
            $(".profileip").eq(2).val(data.last_name);
            $(".profileip").eq(3).val(data.useraddress.contact_address);
            $(".profileip").eq(4).val(data.useraddress.city);
            $(".profileip").eq(5).val(data.useraddress.country);
            $(".profileip").eq(6).val(data.userprofile.pan_number);
            $(".profileip").eq(7).val(data.userprofile.gst_number);
            $(".profileip").eq(8).val(data.userprofile.reference);
        })
        .fail(function (data) {
            console.log(data);
        });
}

function editProfile() {
    $(".editbtn").attr("disabled", true);
    $(".editldr").show();
    var keys = ["firm_name", "first_name", "last_name", "contact_address", "city", "country", "pan_number", "gst_number", "reference"];
    var postdata = {};
    for (var i = 0; i < $(".profileip").length; i++) {
        if ($(".profileip").eq(i).val()) {
            postdata[keys[i]] = $(".profileip").eq(i).val();
        }
    }
    commonAjax(updateprofile_api, 'update', postdata)
        .done(function (data) {
            profile();
            $(".editbtn").attr("disabled", false);
            $(".editldr").hide();
            $("#snackbarsuccs").text("Profile updated");
            showsuccesstoast();
        })
        .fail(function (data) {
            $(".editbtn").attr("disabled", false);
            $(".editldr").hide();
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(
                    JSON.parse(data.responseText)[key] != undefined ?
                    JSON.parse(data.responseText)[key] :
                    JSON.parse(data.responseText)[key].non_field_errors[0]
                );
            }
            showerrtoast();
        });
}

function updateProfilePic() {
    var postdata = new FormData();
    $("#imageUpload")[0].files.length ? postdata.append("profile_pic", $("#imageUpload")[0].files[0]) : '';
    commonAjax(updateprofile_api, 'formUpdate', postdata)
        .done(function (data) {
            profile();
            $("#snackbarsuccs").text("Profile Pic updated");
            showsuccesstoast();
        })
        .fail(function (data) {
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(
                    JSON.parse(data.responseText)[key] != undefined ?
                    JSON.parse(data.responseText)[key] :
                    JSON.parse(data.responseText)[key].non_field_errors[0]
                );
            }
            showerrtoast();
        });
}

function holdlist() {
    $(".holdlist").empty();
    commonAjax(holdlist_api, 'listWithToken')
        .done(function (data) {
            console.log(data);
            $(".nullholdata").remove();
            $(".main_holdtable").show();            
            if (data.length == 0) {
                $(".main_holdtable").hide();
                $(".main_holdrow").append(`<center class="nullholdata"><img src="nodata.png" class="img-responsive"></center>`);
            } else {
                for (var i = 0; i < data.length; i++) {
                    $(".holdlist").append(`
                    <tr>
                        <td>${ i + 1}</td>
                        <td>${data[i].product.stock_id}</td>
                        <td>${date(data[i].created_on)}</td>
                    </tr>
                    `);
                }
            }
        })
        .fail(function (data) {
            console.log(data);
            $(".nullholdata").remove();
            $(".main_holdtable").hide();
            $(".main_holdrow").append(`<center class="nullholdata"><img src="nodata.png" class="img-responsive"></center>`);
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        });
}

var editid;
//enquiry function starts here
function enquiryfunc(val) {
    editid = val;
}

//send enquiry func starts here
function sendenquiry() {

    if ($('#quantity').val() == "") {
        $("#snackbarerror").text("Please enter the quantity");
        showerrtoast();
        event.stopPropagation();
        return;
    }
    if ($('#description').val() == "") {
        $("#snackbarerror").text("Please enter the description");
        showerrtoast();
        event.stopPropagation();
        return;
    }

    var postData = JSON.stringify({
        "description": $('#description').val(),
        "quantity": $('#quantity').val()
    });
    $(".sendldr").show();
    $(".sendbtn").attr("disabled", true);

    $.ajax({
        url: makeenquiry_api + editid + '/?type=3',
        type: 'POST',
        data: postData,
        headers: {
            "Authorization": "Token " + localStorage.wutkn,
            "content-type": 'application/json'
        },
        success: function (data) {
            $(".sendldr").hide();
            $(".sendbtn").attr("disabled", false);
            $('.enquiryclose').click();
            holdlist();
        },
        error: function (data) {
            $(".sendldr").hide();
            $(".sendbtn").attr("disabled", false);
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    })
} //func ends here

function enquirelist() {
    $(".enquirelist").empty();
    commonAjax(enquirelist_api, 'listWithToken')
        .done(function (data) {
            console.log(data);
            $(".nullendata").remove();
            $(".main_enquiretable").show();            
            if (data.length == 0) {
                $(".main_enquiretable").hide();
                $(".main_enquirerow").append(`<center class="nullendata"><img src="nodata.png" class="img-responsive"></center>`);
            } else {
                for (var i = 0; i < data.length; i++) {
                    $(".enquirelist").append(`
                    <tr>
                        <td>${ i + 1}</td>
                        <td>${data[i].stock_id}</td>
                        <td>
                        ${date(data[i].created_on)}
                        </td>
                    </tr>
                    `);
                }
            }
        })
        .fail(function (data) {
            console.log(data);
            $(".nullendata").remove();
            $(".main_enquiretable").hide();
            $(".main_enquirerow").append(`<center class="nullendata"><img src="nodata.png" class="img-responsive"></center>`);
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        });
}

function holdfunc(h_id) {
    // hold_id = h_id;
    var postData = JSON.stringify({
        "solitaires": h_id
    });
    $(".holdloader" + h_id).show();
    $(".holdbtn").attr("disabled", true);

    $.ajax({
        url: holdsolitaries_api,
        type: 'POST',
        data: postData,
        headers: {
            "Authorization": "Token " + localStorage.wutkn,
            "content-type": 'application/json'
        },
        success: function (data) {
            $(".holdloader" + h_id).hide();
            $(".holdbtn").attr("disabled", false);
            $("#snackbarsuccs").text("Your Request Has Been Sent Successfully");
            showsuccesstoast();
            enquirelist();
        },
        error: function (data) {
            $(".holdloader" + h_id).hide();
            $(".holdbtn").attr("disabled", false);
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    })
}

function orders(Url){
    $(".pagination,.orderlist").empty();
    commonAjax(Url, "listWithToken")
        .done(function(data){
            $(".nullorderdata").remove();
            $(".main_ordertable").show();            
            if (data.results.length == 0) {
                $(".main_ordertable").hide();
                $(".main_orderrow").append(`<center class="nullorderdata"><img src="nodata.png" class="img-responsive"></center>`);
            } else {
                for (var i = 0; i < data.results.length; i++) {
                    $(".orderlist").append(`
                    <tr>
                        <td class="slno">1</td>
                        <td>${data.results[i].order_id}</td>
                        <td>
                            <span class="${["orders","order-plcd","order-aprvd","order-cncld","order-cmpltd"][data.results[i].status.id]}">${data.results[i].status.name}</span>
                        </td>
                        <td>
                            <a href="#moreproductsmodal" data-toggle="modal">
                                <i class="fa fa-info-circle infobtn" aria-hidden="true" onclick="orderDetail(${data.results[i].id});"></i>
                            </a>
                        </td>
                    </tr>
                    `);
                }
                pagination(data.next_url, data.prev_url, data.count, data.page_max_size);
                slno(data.page_max_size);
            }
        })
        .fail(function(data){
            $(".nullorderdata").remove();
            $(".main_ordertable").hide();
            $(".main_orderrow").append(`<center class="nullorderdata"><img src="nodata.png" class="img-responsive"></center>`);
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        });
}

function orderDetail(id){
    $(".orderdlist").empty();
    commonAjax(orderdetail_api + id + "/", "listWithToken")
        .done(function(data){
            $(".billaddress").append(`
            <p>${data.payment.billing_address.title} ${data.payment.billing_address.first_name} ${data.payment.billing_address.last_name}</p>
            <p>${data.payment.billing_address.firm_name}</p>                                     
            <p>${data.payment.billing_address.mobile_number}</p>
            <p>${data.payment.billing_address.line1}, ${data.payment.billing_address.line1}, ${data.payment.billing_address.state}, ${data.payment.billing_address.country}, ${data.payment.billing_address.postcode}</p>
            `);
            $(".shipaddress").append(`
            <p>${data.payment.delivery_address.title} ${data.payment.delivery_address.first_name} ${data.payment.delivery_address.last_name}</p>
            <p>${data.payment.delivery_address.firm_name}</p>                                     
            <p>${data.payment.delivery_address.mobile_number}</p>
            <p>${data.payment.delivery_address.line1}, ${data.payment.delivery_address.line1}, ${data.payment.delivery_address.state}, ${data.payment.delivery_address.country}, ${data.payment.delivery_address.postcode}</p>
            `);
            $(".nullorderddata").remove();
            $(".main_orderdtable").show();            
            if (data.order_items.length == 0) {
                $(".main_orderdtable").hide();
                $(".main_orderdrow").append(`<center class="nullorderddata"><img src="nodata.png" class="img-responsive"></center>`);
            } else {
                for (var i = 0; i < data.order_items.length; i++) {
                    $(".orderdlist").append(`
                    <tr>
                        <td>${i+1}</td>
                        <td>
                            <img src="${data.order_items[i].tagged_object.images.length ? data.order_items[i].tagged_object.images[0].image : 'images/solitaries/shape1.png'}" class="img-responsive w50">
                        </td>
                        <td>${data.order_items[i].tagged_object.name}</td>
                        <td>${data.order_items[i].tagged_object.sku}</td>
                        <td>${data.order_items[i].quantity}</td>                        
                        <td>${data.order_items[i].pay_price}</td>                        
                        <td>${data.order_items[i].sub_total}</td>
                        <td>${data.order_items[i].total_price}</td>
                <td> ${data.order_items[i].tagged_object.type != 3 ?`<a class="pointer"  onclick="orderdetailview(${data.order_items[i].id}, '${data.order_items[i].tagged_object.name}');">View Details</a>`: '<a>Not Available</a>'}</td>
                    </tr>
                    `);
                }
                
            }
        })
        .fail(function(data){
            $(".nullorderddata").remove();
            $(".main_orderdtable").hide();
            $(".main_orderdrow").append(`<center class="nullorderddata"><img src="nodata.png" class="img-responsive"></center>`);
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        });
}

//pagination starts here
const pagination = (next, prev, count, size) => {
    $(".pagination").empty();
    var pages = Math.ceil(count / size);
    if (pages > 1) {
        prev ? $(".pagination").append(`<li class="pageprev page-item"><a class="pointer cptr page-link" onclick="prevpage('${prev}')">Previous</a></li>`) : '';
        for (var i = (page_no < 3 ? 1 : page_no - 2); i <= pages && i <= (page_no < 3 ? page_no + (5 - page_no) : page_no + 2); i++) {
            $(".pagination").append(`<li class="pgs pg${i} page-item"><a class="pointer cptr page-link" onclick="navigate(${i})">${i}</a></li>`);
        }
        next ? $(".pagination").append(`<li class="pagenext page-item"><a class="pointer cptr page-link" onclick="nextpage('${next}')">Next</a></li>`) : '';
    }
    $(".pgs").removeClass("active");
    $(".pg" + page_no).addClass("active");
}

function navigate(page){
    page_no = Number(page);
    orders(orders_api + "?page=" + page_no);
}

function nextpage(next){
    page_no++;
    orders(next);
}

function prevpage(prev){
    page_no--;
    orders(prev);
}

function slno(size){
    for(var i =0;i < $(".slno").length;i++){
        $(".slno").eq(i).text(((page_no - 1) * size) + i + 1);
    }
}

function orderdetailview(id, name, type){
    window.location.href = `product-detail.html?details=true&id=${id}&name=${name}`
}
