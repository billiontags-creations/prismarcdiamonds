$(function() {
  $(".ldmldr, .ldmicon").hide();  
  list(0, sessionStorage.jewelcat);
});

var nextUrl ='';
var search = '';

function list(type, cat) {
  if (type == 0) {
    var Url = listjewels_api + sessionStorage.jewelcat + "/" + search;
    $(".list" + cat).empty();
  } else {
    var Url = nextUrl;
  }
  $.ajax({
    url: Url,
    type: "get",
    headers: localStorage.wutkn
      ? { Authorization: "Token " + localStorage.wutkn }
      : {},
    success: function(data) {
      if(data.next_url){
        $(".ldmicon").show();
        nextUrl = data.next_url;
      }else{
        $(".ldmicon").hide();
      }
      $(".nulldata").remove();
      if (data.results.length == 0) {
        $(".list" + sessionStorage.jewelcat).append(`
            <center class="nulldata"><img src="nodata.png" class="img-responsive"></center>
        `);
      } else {
        for (var i = 0; i < data.results.length; i++) {
          $(".list" + cat).append(`
                        <div class="item col-lg-2 col-md-4 col-sm-6  col-xs-12">
                            <div class="product-img position-relative">
                                <a onclick="proddetail(${
                                  data.results[i].id
                                }, '${data.results[i].name}')" class="font16 pointer">
                                    <img class="img-responsive" src="${
                                      data.results[i].images[0].image
                                    }" alt="" style="height:170px;">
                                </a>
                                <div class="icon-wishlist position-absolute ${
                                  data.results[i].is_wish == true
                                    ? "sparkle"
                                    : ""
                                }">
                                    <a class="pointer" onclick="wish(this,${
                                      data.results[i].id
                                    })">
                                        <i class="fa fa-heart ${
                                          data.results[i].is_wish == true
                                            ? "wishtrue"
                                            : ""
                                        }" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="product-content text-item whitebg">
                                <h5>
                                    <a onclick="proddetail(${
                                      data.results[i].id
                                    }, '${data.results[i].name}')" class="font-montserrat font15 pl15 pointer ellipsis">${
            data.results[i].name
          }</a>
                                </h5>
                                <center>
                                    <div class="rating">
                                        <ul class="list-inline mb2 staring${
                                          data.results[i].id
                                        }">
                                        </ul>
                                    </div>
                                </center>
                                <div>
                                    <button class="btn eqbtn"  onclick="proddetail(${
                                      data.results[i].id
                                    }, '${data.results[i].name}')">View Details</button>
                                </div>
                            </div>
                        </div>
                    `);
          starz(data.results[i].rating, data.results[i].id, "staring");
        }
      }
      $(".ldmldr").hide();
    },
    error: function(data) {
      $(".nulldata").remove();
      $(".list" + cat).append(`
                    <center class="nulldata"><img src="nodata.png" class="img-responsive"></center>
                `);
      for (var key in JSON.parse(data.responseText)) {
        $("#snackbarerror").text(
          JSON.parse(data.responseText)[key] != undefined
            ? JSON.parse(data.responseText)[key]
            : JSON.parse(data.responseText)[key].non_field_errors[0]
        );
      }
      showerrtoast();
    }
  });
}

function proddetail(id, name) {
  window.location.href = "product-detail.html?details=false&id="+ id +"&type=1&name=" + name;
}

function wish(el, id) {
  if(localStorage.wutkn){
    $(el).removeClass("pointer").addClass("onwish");    
    $.ajax({
      url: wishlist_api + id + "/" + "?type=1",
      type: "post",
      headers: {
        "content-type": "application/json",
        Authorization: "Token " + localStorage.wutkn
      },
      success: function(data) {
        if (data.in_wish_list == true) {
          $(el)
            .children()
            .addClass("wishtrue");
          $(el)
            .parent()
            .addClass("sparkle");
          $("#snackbarsuccs").text("Added to wishlist");
          showsuccesstoast();
        } else {
          $(el)
            .children()
            .removeClass("wishtrue");
          $(el)
            .parent()
            .removeClass("sparkle");
          $("#snackbarsuccs").text("Removed from wishlist");
          showsuccesstoast();
        }
        $(".wishdrop").siblings().remove();
        wishdrop();
        $(el).addClass("pointer").removeClass("onwish");    
      },
      error: function(data) {
        $(el).addClass("pointer").removeClass("onwish");            
        for (var key in JSON.parse(data.responseText)) {
          $("#snackbarerror").text(
            JSON.parse(data.responseText)[key] != undefined
              ? JSON.parse(data.responseText)[key]
              : JSON.parse(data.responseText)[key].non_field_errors[0]
          );
        }
        showerrtoast();
      }
    });
  } else{
    window.location.href = "login.html";
  }
}

function loadmore(){
  $(".ldmldr").show();  
  list(1, sessionStorage.jewelcat);  
}

function sortby(el){
  search = "?sort=" + $(el).val();
  list( 0 , sessionStorage.jewelcat);    
}

