$(function() {
    for(var i=5; i<36;i++){
        $(".banglesize").append(`<option value="${i}">${i}</option>`);
    }
    $(".banglesize, .ringsize, .sizediv").hide();
    if(order){
        $(".proddetailtab").removeClass("active");
        $(".orderdetailtab").addClass("active");
        $(".proddetailtab").hide();
        retrieveproducts();
    }else{
        $(".orderdetailtab").hide();        
        loaddetails();            
    }
    $(".mega-menu").mouseover(() => {
        $(".zoomContainer").css("position", "");
    });
    
    $(".mega-menu").mouseleave(() => {
        $(".zoomContainer").css("position", "absolute");
    });
    $(".eWeight").keyup(function(event) {
        $(this).val() ? $(this).removeClass("iserr") : $(this).addClass("iserr");
    });    
});

var detail = ["sku", "height", "width", "length","plating", "finding", "diamond_weight", "no_diamond", "colour_stones_weight", "no_colour_stones"];

var pricing = ["gold", "diamond", "stones", "making_charge", "sub_total", "taxes_duties", "total_cost", "advance_pay"];

var productData = {};
productData.type = window.location.search.replace("?","").split("&")[2].split("=")[1]; 

var retrieveId = window.location.search.replace("?","").split("&")[1].split("=")[1] + "/";

var order = JSON.parse(window.location.search.replace("?","").split("&")[0].split("=")[1]);

function retrieveproducts() {
    commonAjax((order ? retrieveorder_api : retriveproduct_api) + retrieveId, order ? 'list' : 'postList', order ? '' : productData)
    .done(function(data) {
        if(order){
            $(".wishicon").hide();
            productmaindetails(data.tagged_object);
            orderdetails(data);
        } else{
            $(".wishicon").attr("onclick", `wish(this, ${data.id})`);            
            productmaindetails(data);
            buyandreviewdetails(data);
            relatedproducts(data);
            $(".shipdate").text("Ships By " + date(data.shipping_date));
        }
        pricebreakup(data);
        productDesc(data);
        $('#preloader').fadeOut('slow', function () {
            $(this).remove();
        });
    })
    .fail(function(data) {
        $(".nulldata").remove();
        $(".list_related").append(`
                <center class="nulldata"><img src="nodata.png"></center>
            `);
        for (var key in JSON.parse(data.responseText)) {
            $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
        }
        showerrtoast();
    });
}

function orderdetails(data){
    $(".ordergold").text(data.metals.name);
    $(".ordercarat").text(data.metals.melting + " Kt");
    $(".orderclarity").text(data.clarity);
    $(".ordercolor").text(data.color);
    $(".orderrate").text(data.metals.rate_on + "%");
    $(".orderpercent").text(data.metals.gold_percent + "%");
    $(".orderweight").text(data.metals.weight + " gms");
}

function wish(el, id) {
    if(localStorage.wutkn){
        $(el).removeClass("pointer").addClass("onwish");    
        $.ajax({
            url: wishlist_api + id + "/" + "?type=" + productData.type,
            type: 'post',
            headers: {
                "content-type": "application/json",
                "Authorization": "Token " + localStorage.wutkn
            },
            success: function(data) {
                if (data.in_wish_list == true) {
                    $(el).children().addClass('wishtrue');
                    $(el).parent().addClass('sparkle');
                    $("#snackbarsuccs").text("Added to wishlist");
                    showsuccesstoast();

                } else {
                    $(el).children().removeClass('wishtrue');
                    $(el).parent().removeClass('sparkle');
                    $("#snackbarsuccs").text("Removed from wishlist");
                    showsuccesstoast();
                }
                $(".wishdrop").siblings().remove();
                wishdrop();
                $(el).addClass("pointer").removeClass("onwish");    
            },
            error: function(data) {
                $(el).addClass("pointer").removeClass("onwish");    
                for (var key in JSON.parse(data.responseText)) {
                    $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
                }
                showerrtoast();
            }
        });
    } else{
        window.location.href = "login.html";
    }
}

function addreview() {
    if(localStorage.wutkn){
        if ($(".rating_val input:checked").length == 0) {
            // $(".rating_val input:").addClass("iserr");
            $("#snackbarerror").text("Rating is required");
            showerrtoast();
            event.stopPropagation();
            return;
        }
        if ($(".reviewcomm").val() == "") {
            $(".reviewcomm").addClass("iserr");
            $("#snackbarerror").text("Review description is required");
            showerrtoast();
            event.stopPropagation();
            return;
        }
        var postData = JSON.stringify({
            rating: $(".rating_val input:checked").val() / 2,
            comment: $(".reviewcomm").val(),
            type: productData.type
        });
        $.ajax({
            url: addreview_api + retrieveId,
            type: "post",
            data: postData,
            headers: {
                "content-type": "application/json",
                "Authorization": "Token " + localStorage.wutkn
            },
            success: function(data) {
                $(".listreviews").append(`
                        <li>
                            <div class="user-comments">
                                <h4 class="heading-regular font15 font-montserrat text-normal" style="width: 260px;"> ${JSON.parse(localStorage.userdetails).first_name} <span style="float: right;"> ${data.rating} <a class="fa fa-star"></a></span></h4>
                                <h6 class="font-montserrat-light font13">${data.comment}</h6>
                            </div>
                        </li>
                    `);
                $(".clsbtn").click();
                $("#snackbarsuccs").text("Review added succesfully");
                showsuccesstoast();

            },
            error: function(data) {
                for (var key in JSON.parse(data.responseText)) {
                    $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
                }
                showerrtoast();
            }
        });
    } else{
        window.location.href = "login.html"; 
    }
}


function proddetail(id, name) {
    window.location.href = "product-detail.html?details=false&id="+ id +"&type="+ productData.type +"&name=" + name;
}

function loaddetails(){
    $(".claritydetails, .colordetails").empty();
    $.get(loaddetails_api)
    .done(function(data){
        for(var i=0; i< data.selected_clarity.length;i++){
            $(".claritydetails").append(`<label class="radio-inline radiolabel">
        <input type="radio" name="optradio2" class="clarityval clarityradio${data.selected_clarity[i].id}"  value="${data.selected_clarity[i].id}" onclick="clarityprice(${data.selected_clarity[i].id});">${data.selected_clarity[i].name}</label>`);
        }
        for(var i=0; i< data.selected_color.length;i++){
            $(".colordetails").append(`<label class="radio-inline radiolabel">
        <input type="radio" name="optradio3" class="colorval colorradio${data.selected_color[i].id}"  value="${data.selected_color[i].id}" onclick="colorprice(${data.selected_color[i].id})">${data.selected_color[i].name}</label>`);
        }
        retrieveproducts();
    })
}

function metalprice(id){
    productData.gold = id;
    retrieveproducts();    
}

function clarityprice(id){
    productData.clarity = id;
    retrieveproducts();        
}

function colorprice(id){
    productData.color = id;
    retrieveproducts();
}

function viewpricing(){
    $(".pricetab").click();
    $("html").animate({scrollTop: 980}, 700);
}

function enquireNow(id, type, cat){
    if(localStorage.wutkn){
        if ($('#edescription').val() == "") {
            $("#snackbarerror").text("Please enter the description");
            showerrtoast();
            event.stopPropagation();
            return;
        }
        var postdata = {
            "description": $('#edescription').val(),
            "quantity": $('.prodQty').val(),
            "clarity": $(".clarityval:checked").val(),
            "color": $(".colorval:checked").val(),
            "carat": $(".caratval:checked").val(),
            "type": type
        };
        cat == 1 || cat == 2 ? postdata.size = cat == 1 ? $(".ringsize").val() : $(".banglesize").val() : '';
        $(".enquirebtn").attr("disabled", true);
        $(".enquireldr").show();
        commonAjax(makeenquiry_api+ id +"/", 'post', postdata)
        .done(function(data){
            $(".enquirebtn").attr("disabled", false);
            $(".enquireldr").hide();
            $(".close").click();
            $("#snackbarsuccs").text("Enquiry succesfully");
            showsuccesstoast();
        })
        .fail(function(data){
            $(".enquirebtn").attr("disabled", false);
            $(".enquireldr").hide();        
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        });
    } else{
        window.location.href = "login.html";
    }
}

function addToCart(id, type, cat){
    if(localStorage.wutkn){
        var postdata = {
            "product_id": id,
            "description": $('#edescription').val(),
            "quantity": $('.prodQty').val(),
            "clarity": $(".clarityval:checked").val(),
            "color": $(".colorval:checked").val(),
            "metals": $(".caratval:checked").attr("data-melting"),
            "product_type": type
        };
        cat == 1 || cat == 2 ? postdata.size = cat == 1 ? $(".ringsize").val() : $(".banglesize").val() : '';
        $(".enquirebtn").attr("disabled", true);
        $(".cartldr").show();
        commonAjax(cart_api, 'post', postdata)
        .done(function(data){
            $(".cartdrop").siblings().remove();
            cartlist(2);
            $(".cartbtn").attr("disabled", false);        
            if(buy){
                window.location.href = "checkout.html"
            }else{
                $(".cartldr").hide();
                $(".cartbtn").text("IN CART");
                $("#snackbarsuccs").text("Added to cart successfully");
                showsuccesstoast();
            }
        })
        .fail(function(data){
            $(".cartbtn").attr("disabled", false);
            $(".cartldr").hide();        
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        });
    } else{
        window.location.href = "login.html";        
    }
}

var buy = false; 

function buynow(cart){
  if(localStorage.wutkn){
    buy = true;
    cart ? window.location.href = "checkout.html" : $(".cartbtn").click();
  }else{
    window.location.href = "login.html";
  }
}

function productmaindetails(data){
    $(".prodname").text(data.name);    
    //breadcrumbs section
    $(".woocommerce-breadcrumb").find('span').text(data.name);
    if(data.category){
        data.category.id == 1 ? $(".ringsize, .sizediv").show() : '';
        data.category.id == 2 ? $(".banglesize, .sizediv").show() : '';
        $(".breadcat").text(data.category.name);
        $(".breadcat").attr('onclick', `getid(${data.category.id})`);
        $(".breadsubcat").text(data.category.name);
    }else{
        $(".breadcat").text('Watches');                
        $(".breadcat").attr('href', 'watches.html');                
    }
    //Images section 
    $("#zoom").attr("src", data.images[0].image);
    $("#zoom").elevateZoom();
    for (var i = 0; i < data.images.length; i++) {
        $(".prodimg" + i).attr("src", data.images[i].image);
        $(".prodimg" + i).attr("data-image", data.images[i].image);
    }
    for (var i = data.images.length; i < 3; i++) {
        $(".prodimg" + i).hide();
    }
    for(var i =0; i < detail.length;i++){
        $(".productcontent").eq(i).text(data[detail[i]] ? data[detail[i]] : 'Not Available');
    }
}

function buyandreviewdetails(data){
    //btn section
    $(".enquirebtn").attr("onclick", `enquireNow(${data.id} , ${productData.type},${data.category ? data.category.id : 0})`);
    $(".buybtn").attr("onclick", `buynow(${data.in_cart});`);
    if(data.in_cart){
        $(".cartbtn").text("IN CART");
    }else{
        $(".cartbtn").text("ADD TO CART");                  
    }
    $(".cartbtn").attr("onclick" , `addToCart(${data.id}, ${productData.type}, ${data.category ? data.category.id : 0})`);
    //details section
    $(".metaldetails, .meltingdetails").empty();            
    for(var i=0; i < data.metal_details.length; i++){
        $(".metaldetails").append(`<label class="radio-inline radiolabel">
        <input type="radio" name="optradio"  ${data.selected_value.gold.gold_id == data.metal_details[i].gold_id ? "checked" : ''} value="${data.metal_details[i].gold_id}" onclick="metalprice(${data.metal_details[i].melting[0].melting_id});">${data.metal_details[i].name}</label>`);
        if(data.selected_value.gold.gold_id == data.metal_details[i].gold_id){
            for(var j=0 ;j < data.metal_details[i].melting.length;j++){
                $(".meltingdetails").append(`<label class="radio-inline radiolabel meltingradio${data.metal_details[i].gold_id} meltingradio">
                <input type="radio" data-melting="${data.metal_details[i].melting[j].id}" class="caratval" ${data.selected_value.gold.melting_id == data.metal_details[i].melting[j].melting_id ? "checked" : ''} name="optradio1" value="${data.metal_details[i].melting[j].melting_id}" onclick="metalprice(${data.metal_details[i].melting[j].melting_id});">${data.metal_details[i].melting[j].melting} Kt</label>`);
            }
        }
    }
    $(".clarityradio" + data.selected_value.diamond.clarity).prop("checked", true);
    $(".colorradio" + data.selected_value.diamond.color).prop("checked", true);
    if (data.review.length == 0) {

    } else {
        $(".listreviews").empty();
        for (let i = 0; i < data.review.length; i++) {
            $(".listreviews").append(`
                <li>
                    <div class="user-comments">
                        <h4 class="heading-regular font15 font-montserrat text-normal" style="width: 260px;"> ${data.review[i].user.first_name} <span style="float: right;"> ${data.review[i].rating} <a class="fa fa-star"></a></span></h4>
                        <h6 class="font-montserrat-light font13">${data.review[i].comment}</h6>
                    </div>
                </li>
            `);
        }
    }
}

function pricebreakup(data){
    if(order){
        $(".prodrangeprice").text("Rs " + data.total_price);
        $(".prodonlineprice").text("Rs " + data.pay_price);
        for(var i =0; i < pricing.length;i++){
            if(i==7){
                $("#pricing-details .productcontent").eq(i).text("Rs " + data.pay_price); 
            } else if(i==2){
                data.pricebreakup[pricing[i]] ? $("#pricing-details .productcontent").eq(i).text("Rs. " + data.pricebreakup[pricing[i]]) : $(".stonespricediv").hide();
            }else{
                $("#pricing-details .productcontent").eq(i).text("Rs. " + data.pricebreakup[pricing[i]]);
            }
        }
    } else{
        $(".prodrangeprice").text("Rs " + data.price_breakup.total_cost);
        $(".prodonlineprice").text("Rs " + data.price_breakup.advance_pay);
        for(var i =0; i < pricing.length;i++){
            if(i==2){
                data.price_breakup[pricing[i]] ? $("#pricing-details .productcontent").eq(i).text("Rs. " + data.price_breakup[pricing[i]]) : $(".stonespricediv").hide();
            } else{
                $("#pricing-details .productcontent").eq(i).text("Rs. " + data.price_breakup[pricing[i]]);
            }   
        }
    }
}

function productDesc(data){
    var stones = order ? data.stone_set : data.stones;
    var diamonds = order ? data.diamond_set : data.diamonds;
    //production description
    if(stones.length == 0){
        $(".stonetable").hide();
    } else{
        for(var i=0;i< stones.length;i++){
            $(".stonetable").append(`
                <tr>
                    <td>${ i + 1}.</td>
                    <td>${stones[i].type}</td>
                    <td>${stones[i].name}</td>
                    <td>${stones[i].shape}</td>
                    <td>${stones[i].color}</td>
                    <td>${stones[i].color_type}</td>
                    <td>${stones[i].clarity}</td>
                    <td>${stones[i].size}</td>
                    <td>${stones[i].no_stone}</td>
                    <td>${stones[i].weight}</td>
                    <td>${stones[i].setting_type}</td>
                    
                </tr>
            `);
        }
    }
    if(diamonds.length == 0){
        $(".diatable").hide();
    } else{
        for(var i=0;i< diamonds.length;i++){
            $(".diatable").append(`
                <tr>
                    <td>${ i + 1}.</td>
                    <td>${diamonds[i].shape}</td>
                    <td>${diamonds[i].size}</td>
                    <td>${diamonds[i].no_diamonds}</td>
                    <td>${diamonds[i].weight}</td>
                    <td>${diamonds[i].setting_type}</td>
                    <
                </tr>
            `);
        }
    }
}

function relatedproducts(data){
    $(".list_related").empty();
    $(".nulldata").remove();
    if (data.related_product.length == 0) {
        $(".list_related").append(`
            <center class="nulldata"><img src="nodata.png" class="img-responsive"></center>
        `);
    } else {
        $(".list_related").append(`<div class="item col-md-1"></div>`);
        for (var i = 0; i < data.related_product.length; i++) {
            $(".list_related").append(`
                <div class="item col-lg-2 col-md-4 col-sm-6  col-xs-12">
                    <div class="product-img position-relative">
                        <a onclick="proddetail(${data.related_product[i].id}, '${data.related_product[i].name}')" class="font16 pointer">
                            <img class="img-responsive" src="${data.related_product[i].images.length!=0?data.related_product[i].images[0].image:""}" alt="" style="height:170px;">
                        </a>
                        <div class="icon-wishlist position-absolute ${data.related_product[i].is_wish==true?'sparkle':''}">
                            <a class="pointer" onclick="wish(this,${data.related_product[i].id})">
                                <i class="fa fa-heart ${data.related_product[i].is_wish==true?'wishtrue':''}" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>
                    <div class="product-content text-item whitebg pb20">
                        <h5>
                            <a onclick="proddetail(${data.related_product[i].id}, ${data.related_product[i].name})" class="font-montserrat font15 pl15 pointer ellipsis">${data.related_product[i].name}</a>
                        </h5>
                        <center>
                            <div class="rating">
                                <ul class="list-inline mb2 staring${data.related_product[i].id}">

                                </ul>
                            </div>
                        </center>
                    </div>
                </div>
            `);
            starz(data.related_product[i].rating, data.related_product[i].id, 'staring');
        }
        $(".list_related").append(`<div class="item col-md-1"></div>`);
    }
}
