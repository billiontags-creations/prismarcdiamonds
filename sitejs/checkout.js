$(function () {
    listAdress();
    cartlist(1);
});
 
var keys = ["title", "first_name", "last_name", "mobile_number", "firm_name", "line1", "line2", "state", "country", "postcode"];

function listAdress() {
    $(".addresscol").remove();
    commonAjax(shippingAddress_api, 'listWithToken')
        .done(function (data) {
            if (data.length == 0) {
                $(".addressList").prepend(``);
            } else {
                for (var i = 0; i < data.length; i++) {
                    $(".addressList").prepend(`
                        <div class="col-md-4 addresscol">
                            <div class="addrsbox" data-id='${data[i].id}'>
                                <div class="row">
                                    <div class="col-md-12">
                                        <p class="floatright">
                                            <a  onclick="editType(2, ${data[i].id})"><i class="fa fa-pencil" aria-hidden="true"></i></a>&emsp;<a data-target="#addrconfirmationmodal" data-toggle="modal" onclick="getdelid(${data[i].id});"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                        </p>
                                    </div>
                                    <div class="col-md-12 pointer" onclick="chooseadrr(this)">
                                        <p>${data[i].title} ${data[i].first_name} ${data[i].last_name}</p>
                                        <p>${data[i].firm_name}</p>                                     
                                        <p>${data[i].mobile_number}</p>
                                        <p>${data[i].line1}, ${data[i].line1}, ${data[i].state}, ${data[i].country}, ${data[i].postcode}</p>
                                        <p class="editaddr${data[i].id}" style="display: none;">${JSON.stringify(data[i])}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    `);
                }
            }
        })
        .fail(function (data) {
            console.log(data);
        });
}

var addrid;

function addrType(type, id) {
    id ? addrid = id : '';
    type == 1 ? $("#addnewaddressmodal input").val('') : '';
    $(".addrbtn").attr("onclick", `addAddress(${type})`);
    $(".addrModalTitle").text(type < 2 ? "Add Address" : "Edit Address");
    $("#addnewaddressmodal").modal('show');
}

function editType(type, id) {
    var data = JSON.parse($(".editaddr" + id).eq(0).text());
    for (var i = 0; i < keys.length; i++) {
        $(".addrip").eq(i).val(data[keys[i]]);
    }
    addrType(type, id);
}

function addAddress(type) {
    for (var i = 0; i < $(".addrip").length; i++) {
        if ($(".addrip").eq(i).val() == "") {
            $(".addrip").eq(i).addClass("iserr");
            $("#snackbarerror").text($(".addrip").eq(i).prev().text().replace("*", "") + " is required");
            showerrtoast();
            event.stopPropagation();
            return;
        }
        if (i == 3) {
            if ($(".addrip").eq(i).val().length < 10) {
                $(".addrip").eq(i).addClass("iserr");
                $("#snackbarerror").text("Phone Number is Invalid");
                showerrtoast();
                event.stopPropagation();
                return;
            }
        }
    }

    var postData = {};
    for (var i = 0; i < keys.length; i++) {
        postData[keys[i]] = $(".addrip").eq(i).val();
    }
    $(".addrbtn").attr("disabled", true);
    $(".addrldr").show();
    commonAjax(shippingAddress_api + (type == 2 ? addrid + "/" : ""), type == 2 ? 'update' : 'post', postData)
        .done(function (data) {
            console.log(data);
            $("#addnewaddressmodal input").val('');
            listAdress();
            $(".close").click();
            $(".addrbtn").attr("disabled", false);
            $(".addrldr").hide();
        })
        .fail(function (data) {
            $(".addrbtn").attr("disabled", false);
            $(".addrldr").hide();
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(
                    JSON.parse(data.responseText)[key] != undefined ?
                    JSON.parse(data.responseText)[key] :
                    JSON.parse(data.responseText)[key].non_field_errors[0]
                );
            }
            showerrtoast();
        })
}

function getdelid(id) {
    $(".deladdrbtn").attr("onclick", `deleteAddress(${id});`);
}

function deleteAddress(id) {
    $(".deladdrbtn").attr("disabled", true);
    $(".deladdrldr").show();
    commonAjax(shippingAddress_api + id + '/', 'delete')
        .done(function (data) {
            listAdress();
            $(".close").click();
            $(".deladdrbtn").attr("disabled", false);
            $(".deladdrldr").hide();
            $("#snackbarsuccs").text("Address deleted");
            showsuccesstoast();
        })
        .fail(function (data) {
            console.log(data);
            $(".deladdrbtn").attr("disabled", false);
            $(".deladdrldr").hide();
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(
                    JSON.parse(data.responseText)[key] != undefined ?
                    JSON.parse(data.responseText)[key] :
                    JSON.parse(data.responseText)[key].non_field_errors[0]
                );
            }
            showerrtoast();
        });
}

function chooseadrr(el) {
    $(el).closest(".addrlist").find('.addrsbox').removeClass("choosenaddr");
    $(el).closest(".addrsbox").addClass("choosenaddr");
}

function proceedtopay() {
    if ($("#pan").val() == '' && Number($(".totalCart").text()) > 200000) {
        $("#pan").addClass("iserr");
        $("#snackbarerror").text("PAN Number is Required");
        showerrtoast();
        event.stopPropagation();
        return;
    }
    if ($("#gst").val() == '') {
        $("#gst").addClass("iserr");
        $("#snackbarerror").text("GST Number is Required");
        showerrtoast();
        event.stopPropagation();
        return;
    }
    if ($(".billiaddr .choosenaddr").length == 0) {
        $("#snackbarerror").text("Billing Address is Required");
        showerrtoast();
        event.stopPropagation();
        return;
    }
    if ($(".deliaddr .choosenaddr").length == 0) {
        $("#snackbarerror").text("Delivery Address is Required");
        showerrtoast();
        event.stopPropagation();
        return;
    }
    var postData = {
        "billing_address": $(".billiaddr .choosenaddr").attr('data-id'),
        "gst_number": $("#gst").val(),
        "delivery_address": $(".deliaddr .choosenaddr").attr('data-id'),
    };
    $("#pan").val() ? postData.pan_number = $("#pan").val() : '';
    commonAjax(payment_api, 'post', postData)
        .done(function (data) {
            console.log(data);
            var options = {
                "key": "rzp_test_twhGB9MZHnXYT0",
                "amount": data.amount,
                "name": "Prismarc",
                "description": "Buy now",
                "image": "http://prismarc.billioncart.in/images/prismarc/prismarc-logo.svg",
                "order_id": data.order_id,
                "currency": "INR",
                "handler": function (response) {
                    // console.log(response);
                    console.log(response.razorpay_payment_id);
                    console.log(response.razorpay_order_id);
                    console.log(response.razorpay_signature);

                    var postData = JSON.stringify({
                        "razorpay_payment_id": response.razorpay_payment_id,
                        "razorpay_order_id": response.razorpay_order_id,
                        "razorpay_signature": response.razorpay_signature
                    });

                    $.ajax({
                        url: paymentsuccess_api,
                        type: 'POST',
                        data: postData,
                        headers: {
                            "content-type": 'application/json',
                            "Authorization": "Token " + localStorage.wutkn
                        },
                        success: function (data) {
                            $("#snackbarsuccs").text("Payment Success");
                            showsuccesstoast();
                            window.location.href = "myprofile.html";
                        },
                        error: function (data) {
                            console.log("Error Occured in Online Buynow after razorpay");
                            var errtext = "";
                            for (var key in JSON.parse(data.responseText)) {
                                errtext = JSON.parse(data.responseText)[key][0];
                            }
                            $(".paymentldr").hide();
                            $(".paymentBtn").attr("disabled", false);
                            $("#snackbarerror").text('Payment failure');
                            showerrtoast();

                        }
                    }).done(function (dataJson) {});
                },
                "prefill": {
                    "name": JSON.parse(localStorage.userdetails).first_name + JSON.parse(localStorage.userdetails).last_name,
                    "email": JSON.parse(localStorage.userdetails).email,
                    "contact": JSON.parse(localStorage.userdetails).username
                },
                "notes": {
                    "address": "Prismarc Payment"
                },
                "theme": {
                    "color": "#0058ae"
                }
            };

            rzp1 = new Razorpay(options);

            rzp1.open();
        })
        .fail(function (data) {
            console.log(data);
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(
                    JSON.parse(data.responseText)[key] != undefined ?
                    JSON.parse(data.responseText)[key] :
                    JSON.parse(data.responseText)[key].non_field_errors[0]
                );
            }
        });
}