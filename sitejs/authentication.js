$(function () {
  $(".registerldr,.loginldr,.forgotldr,.resetldr").hide();
  $(".loginip").keyup(function (event) {
    $(this).val() ? $(this).removeClass('iserr') : $(this).addClass('iserr');
    event.keyCode == 13 ? $(".loginbtn").click() : "";
  });
  $(".forgotphone").keyup(function (event) {
    $(this).val() ? $(this).removeClass('iserr') : $(this).addClass('iserr');
    event.keyCode == 13 ? $(".forgotbtn").click() : "";
  });
});

function register() {
  for (var i = 1; i < 10; i++) {
    if ($("#registerfield" + i).val() == "") {
      $("#registerfield" + i).addClass("iserr");
      $("#snackbarerror").text($("#registerfield" + i).attr('placeholder').slice(10) + " is required");
      showerrtoast();
      event.stopPropagation();
      return;
    }
    if (i == 3) {
      var x = $("#registerfield" + 3).val();
      var atpos = x.indexOf("@");
      var dotpos = x.lastIndexOf(".");
      if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= x.length) {
        $("#registerfield" + 3).addClass("iserr");
        $("#snackbarerror").text("Valid E-Mail Address is required");
        showiperrtoast();
        event.stopPropagation();
        return;
      }
    }
    if (i == 5) {
      if ($("#registerfield" + i).val() == "") {
        $("#registerfield" + i).addClass("iserr");
        $("#snackbarerror").text("Contact Address is required");
        showerrtoast();
        event.stopPropagation();
        return;
      }
    }
    if (i == 9) {
      if ($("#registerfield" + i).val() == "") {
        $("#registerfield" + i).addClass("iserr");
        $("#snackbarerror").text("Conform password is required");
        showerrtoast();
        event.stopPropagation();
        return;
      } else if ($("#registerfield8").val() != $("#registerfield9").val()) {
        // $("#registerfield" + i).addClass("iserr");
        $("#snackbarerror").text("Passwords must be same");
        showerrtoast();
        event.stopPropagation();
        return;
      }
    }
  }

  $(".registerldr").show();
  $(".registerbtn").attr("disabled", true);
  var postData = {
    "password": $('#registerfield9').val(),
    "username": $('#registerfield4').val(),
    "useraddress": {
      "country": $('#registerfield7').val(),
      "contact_address": $('#registerfield5').val(),
      "city": $("#registerfield6").val(),
    },
    "email": $("#registerfield3").val(),
    "first_name": $("#registerfield1").val(),
    "last_name": $("#registerfield2").val(),
    "userprofile": {}
  };
  $("#firm_name").val() ? postData.userprofile.firm_name = $("#firm_name").val() : '';
  $("#pan").val() ? postData.userprofile.pan_number = $("#pan").val() : '';
  $("#gst").val() ? postData.userprofile.gst_number = $("#gst").val() : '';
  $("#reference").val() ? postData.userprofile.reference = $("#reference").val() : '';
  $.ajax({
    url: registeration_api,
    type: 'post',
    data: JSON.stringify(postData),
    headers: {
      "content-type": 'application/json',
    },
    success: function (data) {
      $(".registerldr").hide();
      $(".registerbtn").attr("disabled", false);
      $("#snackbarsuccs").text("You have been registered successfully,wait for your activation!!!");
      showsuccesstoast();
      setTimeout(function () {
        window.location.href = "index.html"
      }, 2000);
    },
    error: function (data) {
      $(".registerldr").hide();
      $(".registerbtn").attr("disabled", false);
      for (var key in JSON.parse(data.responseText)) {
        if ((JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0])[0] == "A user with that username already exists.") {
          $("#snackbarerror").text((JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0])[0]);
        } else if ((JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0])[0] == undefined) {
          if ((JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]).non_field_errors[0] == "An account with this email already exists.") {
            $("#snackbarerror").text((JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]).non_field_errors[0]);
          } else {
            $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
          }
        }
        showerrtoast();
      }
      showerrtoast();
    }
  })
}

//LOGIN FUNCTION STARTS HERE
function login() {

  if ($(".loginName").val() == "") {
    $(".loginName").addClass("iserr");
    $("#snackbarerror").text("E-Mail Address / Phone No is required");
    showerrtoast();
    event.stopPropagation();
    return;
  }

  var numberRegex = /^[+-]?\d+(\.\d+)?([eE][+-]?\d+)?$/;
  var str = $('.loginName').val();
  if (numberRegex.test(str)) {
    var phone = $('.loginName').val();
    var phoneNum = phone.replace(/[^\d]/g, '');
    if (phoneNum.length < 10 || phoneNum.length > 11) {
      $('.loginName').addClass("iserr");
      $("#snackbarerror").text("Valid Phone No is required");
      showiperrtoast();
      event.stopPropagation();
      return;
    }
  } else {
    var x = $('.loginName').val();
    var atpos = x.indexOf("@");
    var dotpos = x.lastIndexOf(".");
    if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= x.length) {
      $('.loginName').addClass("iserr");
      $("#snackbarerror").text("Valid E-Mail Address is required");
      showiperrtoast();
      event.stopPropagation();
      return;
    }
  }

  if ($(".loginPassword").val() == "") {
    $(".loginPassword").addClass("iserr");
    $("#snackbarerror").text("Password is required");
    showerrtoast();
    event.stopPropagation();
    return;
  }

  if ($(".loginPassword").val().length < 6) {
    $(".loginPassword").addClass("iserr");
    $("#snackbarerror").text("Atleast 6 characters is required for password");
    showerrtoast();
    event.stopPropagation();
    return;
  }

  $(".loginldr").show();
  $(".loginbtn").attr("disabled", true);

  var uniqueclient = Math.random() * 10000000000000000;
  var postData = JSON.stringify({
    "username": $('.loginName').val(),
    "password": $('.loginPassword').val(),
    "client": uniqueclient,
    "role": 1
  });

  $.ajax({
    url: login_api,
    type: 'post',
    data: postData,
    headers: {
      "content-type": 'application/json',
    },
    success: function (data) {
      $(".loginldr").hide();
      $(".loginbtn").attr("disabled", false);
      localStorage.userdetails = JSON.stringify(data);
      localStorage.user_name = data.first_name;
      localStorage.wutkn = data.token;
      localStorage.wish_count = data.wish_count;
      loginsuccess();
    },
    error: function (data) {
      $(".loginldr").hide();
      $('.authcls').click();
      $(".loginbtn").attr("disabled", false);
      for (var key in JSON.parse(data.responseText)) {
        $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
      }
      showerrtoast();
    }

  });
}

function loginsuccess() {
  setTimeout(function () {
    if (localStorage.pageid_red == 0) {
      window.location.href = "index.html";
    } else if (localStorage.pageid_red == 1) {
      window.location.href = "index.html";
    } else if (localStorage.pageid_red == 2) {
      window.location.href = "register.html";
    } else if (localStorage.pageid_red == 3) {
      window.location.href = "contactus.html";
    } else if (localStorage.pageid_red == 4) {
      window.location.href = "our-expertise.html";
    } else if (localStorage.pageid_red == 5) {
      window.location.href = "our-company.html";
    } else if (sessionStorage.jewelcat == 4 || localStorage.pageid_red == 6) {
      window.location.href = "necklaces.html"
    } else if (sessionStorage.jewelcat == 1 || localStorage.pageid_red == 7) {
      window.location.href = "rings.html"
    } else if (sessionStorage.jewelcat == 3 || localStorage.pageid_red == 8) {
      window.location.href = "earrings.html"
    } else if (sessionStorage.jewelcat == 2 || localStorage.pageid_red == 9) {
      window.location.href = "bangles.html"
    } else if (sessionStorage.jewelcat == 5 || localStorage.pageid_red == 10) {
      window.location.href = "pendants.html"
    } else if (localStorage.pageid_red == 11) {
      window.location.href = "solitaries.html"
    } else if (localStorage.pageid_red == 12) {
      window.location.href = "watches.html"
    } else {
      window.location.href = "index.html";
    }
  }, 2000);

}

//FORGOT PASSWORD FUNCTION STARTS HERE
function forgotpassword() {

  if ($(".forgotphone").val() == "") {
    $(".forgotphone").addClass("iserr");
    $("#snackbarerror").text("Phone number is required");
    showerrtoast();
    event.stopPropagation();
    return;
  }

  var numberRegex = /^[+-]?\d+(\.\d+)?([eE][+-]?\d+)?$/;
  var str = $('.forgotphone').val();
  if (numberRegex.test(str)) {
    var phone = $('.forgotphone').val();
    var phoneNum = phone.replace(/[^\d]/g, '');
    if (phoneNum.length < 10 || phoneNum.length > 11) {
      $('.forgotphone').addClass("iserr");
      $("#snackbarerror").text("Valid Phone No is required");
      showiperrtoast();
      event.stopPropagation();
      return;
    }
  }

  $(".forgotldr").show();
  $(".forgotbtn").attr("disabled", true);
  var postData = JSON.stringify({
    "mobile_number": $('.forgotphone').val()
  });

  $.ajax({
    url: forgotpassword_api,
    type: 'post',
    data: postData,
    headers: {
      "content-type": 'application/json',
    },
    success: function (data) {
      $(".forgotldr").hide();
      $(".forgotbtn").attr("disabled", false);
      sessionStorage.phone = data.mobile_number;
      $("#snackbarsuccs").text("An otp is sent to your phone");
      showsuccesstoast();
      setTimeout(function () {
        window.location.replace("reset-password.html")
      }, 2000);
    },
    error: function (data) {
      $(".forgotldr").hide();
      $(".forgotbtn").attr("disabled", false);
      for (var key in JSON.parse(data.responseText)) {
        $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
      }
      showerrtoast();
    }
  }); //ajax ends here
} //function ends here

//function reset starts here
function reset() {

  if ($(".resetotp").val() == "") {
    $(".resetotp").addClass("iserr");
    $("#snackbarerror").text("otp is required");
    showerrtoast();
    event.stopPropagation();
    return;
  }

  if ($(".resetpass").val() == "") {
    $(".resetpass").addClass("iserr");
    $("#snackbarerror").text("Password is required");
    showerrtoast();
    event.stopPropagation();
    return;
  }

  if ($(".resetpass").val().length < 6) {
    $(".resetpass").addClass("iserr");
    $("#snackbarerror").text("Atleast 6 characters is required for password");
    showerrtoast();
    event.stopPropagation();
    return;
  }

  if ($(".resetretype").val() == "") {
    $(".resetretype").addClass("iserr");
    $("#snackbarerror").text("Retype your pasword");
    showerrtoast();
    event.stopPropagation();
    return;
  }

  if ($(".resetretype").val().length < 6) {
    $(".resetretype").addClass("iserr");
    $("#snackbarerror").text("Atleast 6 characters is required for retype password");
    showerrtoast();
    event.stopPropagation();
    return;
  }

  if ($(".resetretype").val() != $(".resetpass").val()) {
    $(".resetretype").addClass("iserr");
    $("#snackbarerror").text("Retyped pasword is incorrect");
    showerrtoast();
    event.stopPropagation();
    return;
  }

  $(".resetldr").show();
  $(".resetbtn").attr("disabled", true);
  var uniqueclient = Math.random() * 10000000000000000;
  var postData = JSON.stringify({
    "mobile_number": sessionStorage.phone,
    "otp": $(".resetotp").val(),
    "client": uniqueclient,
    "password": $(".resetpass").val()
  });

  $.ajax({
    url: resetpass_api,
    type: 'post',
    data: postData,
    headers: {
      "content-type": 'application/json',
    },
    success: function (data) {
      console.log(data);
      $(".resetldr").hide();
      $(".resetbtn").attr("disabled", false);
      localStorage.userdetails = JSON.stringify(data);
      localStorage.user_name = data.first_name;
      localStorage.wutkn = data.token;
      $("#snackbarsuccs").text("Password resetted successfully!!!");
      showsuccesstoast();
      loginsuccess();      
    },
    error: function (data) {
      $(".resetldr").hide();
      $(".resetbtn").attr("disabled", false);
      for (var key in JSON.parse(data.responseText)) {
        $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
      }
      showerrtoast();
    }
  }); //ajax ends here
} //function ends here


$("input,textarea").mouseover(function () {
  $(this).removeClass('iserr');
})

function changepassword() {
  for (var i = 0; i < $(".changeip").length; i++) {
    if ($(".changeip").eq(i).val() == "") {
      $(".changeip").eq(i).addClass("iserr");
      $("#snackbarerror").text($(".changeip").eq(i).attr("placeholder").replace("Enter ", "") + " is required");
      showerrtoast();
      event.stopPropagation();
      return;
    }
    if ($(".changeip").eq(i).val().length < 6) {
      $(".changeip").eq(i).addClass("iserr");
      $("#snackbarerror").text($(".changeip").eq(i).attr("placeholder").replace("Enter ", "") + " is too Short");
      showerrtoast();
      event.stopPropagation();
      return;
    }
    if ($(".changeip").eq(0).val() == $(".changeip").eq(1).val()) {
      $(".changeip").eq(1).addClass("iserr");
      $("#snackbarerror").text("New password is same as Old pasword");
      showerrtoast();
      event.stopPropagation();
      return;
    }
    if ($(".changeip").eq(1).val() != $(".changeip").eq(2).val()) {
      $(".changeip").eq(2).addClass("iserr");
      $("#snackbarerror").text("Re enter your password correctly");
      showerrtoast();
      event.stopPropagation();
      return;
    }
  }
  var postData = {
    "password": $(".changeip").eq(0).val(),
    "new_password": $(".changeip").eq(2).val()
  };
  $(".changebtn").attr("disabled", true);
  $(".changeldr").show();
  commonAjax(changepwd_api, 'post', postData)
    .done(function (data) {
      $(".changebtn").attr("disabled", false);
      $(".changeldr").hide();
      $("#snackbarsuccs").text("Password Changed successfully!!!");
      showsuccesstoast();
    })
    .fail(function (data) {
      $(".changebtn").attr("disabled", false);
      $(".changeldr").hide();
      for (var key in JSON.parse(data.responseText)) {
        $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
      }
      showerrtoast();
    });
}