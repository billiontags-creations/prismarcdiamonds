$(".sendldr").hide();

//success toast fn starts here
function showsuccesstoast() {
  var x = document.getElementById("snackbarsuccs");
  x.className = "show";
  setTimeout(function() {
    x.className = x.className.replace("show", "");
  }, 4000);
}

//failure toast fn starts here
function showerrtoast() {
  var x = document.getElementById("snackbarerror");
  x.className = "show";
  setTimeout(function() {
    x.className = x.className.replace("show", "");
  }, 4000);
}

//error toast for ip errors
function showiperrtoast() {
  var x = document.getElementById("snackbarerror");
  x.className = "show";
  setTimeout(function() {
    x.className = x.className.replace("show", "");
  }, 3500);
}

$(function() {
  $("body").append(
    '<div id="snackbarsuccs"></div><div id="snackbarerror"></div>'
  );
  $(".manufacturingprice,.manufacturingpricedit").keypress(function(e) {
    if (
      $(this).val().length > 5 ||
      (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57))
    ) {
      return false;
    }
  });
  $(".newcart").remove();
  $(".box__item ul").eq(0).after(`
  <ul class="list-inline group-social newcart">
  <li class="top-mini-cart">
      <a href="javascript:void(0)" class="baskets dropdown-toggle" title="cart" data-toggle="dropdown">
          <span class="number-cart position-relative">
          
              <i class="fa fa-shopping-basket carticon"></i>
              <span class="number-items position-cart position-absolute cartcount">0</span>
          </span>
      </a>
      <div class="cart-dd dropdown-menu">
        <div id="cart-info">
          <div id="cart-content" class="cart-content ">
            <div class="mini-cart-action cartdrop">
              <a href="cart.html" class="btn-checkout font-montserrat text-normal text-uppercase" onclick="">View Cart</a>
            </div>
          </div>
        </div>
      </div>
    </li>
</ul>                
  `);
  if (localStorage.wutkn) {
        droping();
        $("#iconid").attr("onclick", "window.location.href='myprofile.html'")
  } else {
    $(".cart-dd").hide();
    $(".number-items").text(0);    
  }
  $(".btnldr").hide();
});

function droping(){
  $(".wishdrop")
  .siblings()
  .remove();
  $(".cartdrop")
  .siblings()
  .remove();
  wishdrop();
  cartlist(2);
}

//logout fn starts here
function logout() {
  $.ajax({
    url: logout_api,
    type: "post",
    headers: {
      "content-type": "application/json"
    },
    beforeSend: function(xhr) {
      xhr.setRequestHeader("Authorization", "Token " + localStorage.wutkn);
    },
    success: function(data) {
      sessionStorage.clear();
      localStorage.clear();
    },
    error: function(data) {
      sessionStorage.clear();
      localStorage.clear();
      window.location.replace("index.html");
    }
  }).done(function(dataJson) {
    window.location.replace("index.html");
  });
} //logout fn starts here

if (localStorage.wutkn == undefined || localStorage.wutkn == "") {
  $("#welcomeid").hide();
  $("#registerid").show();
  $("#poweroff").hide();
} else {
  $("#welcomeid").show();
  $("#registerid").hide();
  $("#poweroff").show();
}

//solitaries function starts here
function solitaries(type) {
  // if (localStorage.wutkn == undefined || localStorage.wutkn == '') {
  //     if (type == 1) {
  //         $("#snackbarerror").text('Please login to view this page');
  //         showerrtoast();
  //         setTimeout(function() {
  //             window.location.href = "index.html"
  //         }, 3000);
  //     } else if (type == 2) {
  //         $("#snackbarerror").text('Please login to view this page');
  //         showerrtoast();
  //     }
  //
  // } else {
  localStorage.pageid_red = type;
  window.location.href = "solitaries.html";
  // }
}

//jewellery function starts here
function jewelleries(type) {
  // if (localStorage.wutkn == undefined || localStorage.wutkn == '') {
  //     if (type == 1) {
  //         $("#snackbarerror").text('Please login to view this page');
  //         showerrtoast();
  //         setTimeout(function() {
  //             window.location.href = "index.html"
  //         }, 3000);
  //     } else if (type <= 7) {
  //         $("#snackbarerror").text('Please login to view this page');
  //         showerrtoast();
  //     }
  //
  // } else {
  if (type == 3) {
    sessionStorage.jewelcat = 4;
    localStorage.pageid_red = 6;
    window.location.href = "necklaces.html";
  } else if (type == 4) {
    sessionStorage.jewelcat = 1;
    localStorage.pageid_red = 7;
    window.location.href = "rings.html";
  } else if (type == 5) {
    sessionStorage.jewelcat = 3;
    localStorage.pageid_red = 8;
    window.location.href = "earrings.html";
  } else if (type == 6) {
    sessionStorage.jewelcat = 2;
    localStorage.pageid_red = 9;
    window.location.href = "bangles.html";
  } else if (type == 7) {
    sessionStorage.jewelcat = 5;
    localStorage.pageid_red = 10;
    window.location.href = "pendants.html";
  } else {
    window.location.href = "jewelleries.html";
  }
}
// }

//jewellery function starts here
function watches(type) {
  // if (localStorage.wutkn == undefined || localStorage.wutkn == '') {
  //     if (type == 1) {
  //         $("#snackbarerror").text('Please login to view this page');
  //         showerrtoast();
  //         setTimeout(function() {
  //             window.location.href = "index.html"
  //         }, 3000);
  //     } else if (type == 2) {
  //         $("#snackbarerror").text('Please login to view this page');
  //         showerrtoast();
  //     }
  //
  // } else {
  localStorage.pageid_red = 12;
  window.location.href = "watches.html";
}
// }

//function for newletter validation starts here
function newsletter() {
  if ($(".newsletteremailid").val() == "") {
    $(".newsletteremailid").addClass("iserr");
    $("#snackbarerror").text("Email Id is Required");
    showerrtoast();
    event.preventDefault();
    return;
  } else {
    var emailid = $(".newsletteremailid").val();
    if (
      emailid.indexOf("@") < 1 ||
      emailid.lastIndexOf(".") < emailid.indexOf("@") + 2 ||
      emailid.lastIndexOf(".") + 2 >= emailid.length
    ) {
      $(".newsletteremailid").addClass("iserr");
      $("#snackbarerror").text("valid Email-Id is required");
      showerrtoast();
      event.stopPropagation();
      return;
    }
  }
  $(".sendldr").show();
  $(".newsltrsendbtn").attr("disabled", true);
  var postData = JSON.stringify({
    email: $(".newsletteremailid").val()
  });
  $.ajax({
    url: newsletter_api,
    type: "post",
    data: postData,
    headers: {
      "content-type": "application/json"
    },
    success: function(data) {
      $("#snackbarsuccs").text("Subscription done successfully");
      showsuccesstoast();
      $(".sendldr").hide();
      $(".newsltrsendbtn").attr("disabled", false);
      $(".newsletteremailid").val("");
    },
    error: function(data) {
      for (var key in JSON.parse(data.responseText)) {
        $("#snackbarerror").text(
          (JSON.parse(data.responseText)[key] != undefined
            ? JSON.parse(data.responseText)[key]
            : JSON.parse(data.responseText)[key].non_field_errors[0])[0]
        );
      }
      showerrtoast();
      $(".sendldr").hide();
      $(".newsltrsendbtn").attr("disabled", false);
    }
  });
} //function for newletter validation ends here

function getid(id) {
  sessionStorage.jewelcat = id;
  switch (id) {
    case 1:
      localStorage.pageid_red = 7;
      window.location.href = "rings.html";
      break;
    case 2:
      localStorage.pageid_red = 9;
      window.location.href = "bangles.html";
      break;
    case 3:
      localStorage.pageid_red = 8;
      window.location.href = "earrings.html";
      break;
    case 4:
      localStorage.pageid_red = 6;
      window.location.href = "necklaces.html";
      break;
    case 5:
      localStorage.pageid_red = 10;
      window.location.href = "pendants.html";
      break;
  }
}

function wishdrop() {
  $.ajax({
    url: listwish_api,
    type: "get",
    headers: {
      Authorization: "Token " + localStorage.wutkn
    },
    success: function(data) {
      console.log(data);
      $(".number-items").eq(1).text(data.length);
      if (data.length != 0) {
        for(var i =0; i< 2 && i< data.length;i++){
          $(".wishdrop").before(`
              <div class="items position-relative mark-border cartdropitems">
                <div class="items-inner">
                    <div class="cart-item-image">
                        <a href="#">
                            <img src="${
                              data[i].tagged_object.image
                            }" alt="" class="cartprodimages">
                        </a>
                    </div>
                    <div class="cart-item-info">
                        <h4 class="font16 cartprodtitle"><a class="product-name font-montserrat"></a></h4>
                        <h5 class="font-montserrat-light mb3">${
                          data[i].tagged_object.name
                        }</h5>
                    </div>
                </div>
            </div>
          `);
        }
      }
    },
    error: function(data) {
      for (var key in JSON.parse(data.responseText)) {
        $("#snackbarerror").text(
          JSON.parse(data.responseText)[key] != undefined
            ? JSON.parse(data.responseText)[key]
            : JSON.parse(data.responseText)[key].non_field_errors[0]
        );
      }
      showerrtoast();
    }
  });
}

function addCart(el, id, type) {
  if (localStorage.wutkn) {
    $(el).removeClass("pointer").addClass("onwish");    
    $.ajax({
      url: cart_api,
      type: "post",
      headers: {
        Authorization: "Token " + localStorage.wutkn,
        "content-type": "application/json"
      },
      data: JSON.stringify({
        product_id: id,
        product_type: type,
        quantity: 1
      }),
      success: function(data) {
        $(".cartdrop")
        .siblings()
        .remove();
        cartlist(2);
      
        $(el).text("IN CART");
        $("#snackbarsuccs").text("Added to Cart");
        showsuccesstoast();
        $(el).addClass("pointer").removeClass("onwish");    
      },
      error: function(data) {
        $(el).addClass("pointer").removeClass("onwish");            
        for (var key in JSON.parse(data.responseText)) {
          $("#snackbarerror").text(
            JSON.parse(data.responseText)[key] != undefined
              ? JSON.parse(data.responseText)[key]
              : JSON.parse(data.responseText)[key].non_field_errors[0]
          );
        }
        showerrtoast();
      }
    });
  } else {
    window.location.href = 'login.html';
  }
}

function cartlist(type) {
  if (localStorage.wutkn) {
    
    $.ajax({
      url: cartlist_api,
      type: "GET",
      headers: {
        Authorization: "Token " + localStorage.wutkn,
        "content-type": "application/json"
      },
      success: function(data) {
        var cartTotal = 0;      
        $(".cartcount").text(data.length);
        $(".cartlist").empty();
        $(".maincarttable").show();
        $('.nullcartdata').remove();
        if (data.length == 0) {
          if (type == 1) {
          $(".totalCart").text(0);            
            $(".maincarttable").hide();
            $(".main_cartrow").append(`
                <center class="nullcartdata"><img src="nodata.png" class="img-responsive"></center>
            `);
          } else {
          }
        } else {
          
          for (var i = 0; type == 1 ? i < data.length : i < 2 && i < data.length; i++) {
            cartTotal = cartTotal + data[i].sub_total;
            type == 1
              ? $(".cartlist").append(`
              <tr>
                  <td>${i + 1}</td>
                  <td>
                      <img src="${data[i].tagged_object.image ? data[i].tagged_object.image : 'images/solitaries/shape1.png'}" class="img-responsive w50">
                  </td>
                  <td>${data[i].tagged_object.name ? data[i].tagged_object.name : data[i].tagged_object.stock_id}</td>
                  <td>${data[i].tagged_object.category ? data[i].tagged_object.category: data[i].tagged_object.type }</td>
                  <td>Rs.${data[i].sub_total}</td>
                  <td>
                      <div class="quantity-product mt10 mb0">
                          <div class="single_input">
                              <div class="quantity">
                                  <input type="text" name="qty" id="qty" maxlength="1" value="${
                                    data[i].quantity == 0 ? 1 : data[i].quantity
                                  }" title="Qty" class="form-control input-desktop qty qty-input cartQty${
                  data[i].id
                }" readonly>
                                  <div class="btn-plus">
                                      <button type="button" class="reduced items position-absolute qty-plus" onclick="increasecart(${
                                        data[i].id
                                      })" ${data[i].tagged_object.stock_id ? "disabled" : ''}>
                                          <span class="js--qty-adjuster js--minus" data-qty="0" data-id="">+</span>
                                      </button>
                                      <button type="button" class="increase items position-absolute qty-minus" onclick="decreasecart(${
                                        data[i].id
                                      })" ${data[i].tagged_object.stock_id ? "disabled" : ''}>
                                          <span class="js--qty-adjuster js--add" data-qty="11" data-id="">-</span>
                                      </button>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </td>
                  <td>Rs. ${data[i].sub_total}</td>
                  <td>
                      <div class="product-remove">
                          <a class="pointer flaticon-cancel font15" data-target="#deletecartmodal" data-toggle="modal"
                              onclick="deletecartid(${data[i].id},1)"></a>
                      </div>
                  </td>
              </tr>
            `)
              :         $(".cartdrop").before(`
              <div class="items position-relative mark-border cartdropitems2">
                <div class="items-inner">
                    <div class="cart-item-image">
                        <a href="#">
                            <img src="${data[i].tagged_object.image ? data[i].tagged_object.image : 'images/solitaries/shape1.png'}"  alt="" class="cartprodimages">
                        </a>
                    </div>
                    <div class="cart-item-info">
                        <h4 class="font16 cartprodtitle"><a class="product-name font-montserrat"></a></h4>
                        <h5 class="font-montserrat-light mb3">${data[i].tagged_object.name ? data[i].tagged_object.name : data[i].tagged_object.stock_id}</h5>
                    </div>
                </div>
            </div>
          `);
          }
          $(".totalCart").text(cartTotal.toFixed(2));
        }
      },
      error: function(data) {
        for (var key in JSON.parse(data.responseText)) {
          $("#snackbarerror").text(
            JSON.parse(data.responseText)[key] != undefined
              ? JSON.parse(data.responseText)[key]
              : JSON.parse(data.responseText)[key].non_field_errors[0]
          );
        }
        showerrtoast();
      }
    });
  }
}

function increasecart(id) {
  $(".cartQty" + id).val() < 9 ? updateCart(id, 1) : "";
}

function decreasecart(id) {
  $(".cartQty" + id).val() > 1 ? updateCart(id, 2) : "";
}

function updateCart(id, type) {
  $.ajax({
    url: cart_api + id + "/",
    type: "patch",
    headers: {
      Authorization: "Token " + localStorage.wutkn,
      "content-type": "application/json"
    },
    data: JSON.stringify({
      quantity:
        type == 1
          ? Number($(".cartQty" + id).val()) + 1
          : Number($(".cartQty" + id).val()) - 1
    }),
    success: function(data) {
      cartlist(1);
    },
    error: function(data) {
      for (var key in JSON.parse(data.responseText)) {
        $("#snackbarerror").text(
          JSON.parse(data.responseText)[key] != undefined
            ? JSON.parse(data.responseText)[key]
            : JSON.parse(data.responseText)[key].non_field_errors[0]
        );
      }
      showerrtoast();
    }
  });
}

function deleteCart(id, type) {
  $(".delcartbtn").attr("disabled", true);
  $(".delcartldr").show();
  $.ajax({
    url: cart_api + id + "/",
    type: "delete",
    headers: {
      Authorization: "Token " + localStorage.wutkn,
      "content-type": "application/json"
    },
    success: function(data) {
      $(".delcartbtn").attr("disabled", false);
      $(".delcartldr").hide();
      $(".cartdrop")
        .siblings()
        .remove();
        cartlist(2);
      type == 1 ? cartlist(1) : "";
      // cartlist(2);
      // $(".cartdrop")
      // .siblings()
      // .remove();
      // cartlist(2);
      $("#snackbarsuccs").text("Item has been removed from your Cart");
      showsuccesstoast();
      $(".delcartcls").click();
    },
    error: function(data) {
      $(".delcartcls").click();
      $(".delcartbtn").attr("disabled", false);
      $(".delcartldr").hide();
      for (var key in JSON.parse(data.responseText)) {
        $("#snackbarerror").text(
          JSON.parse(data.responseText)[key] != undefined
            ? JSON.parse(data.responseText)[key]
            : JSON.parse(data.responseText)[key].non_field_errors[0]
        );
      }
      showerrtoast();
    }
  });
}

function deletecartid(id, type) {
  $(".delcartbtn").attr("onclick", `deleteCart(${id}, ${type})`);
}

function commonAjax(Url, type, Data) {
  var settings = {
      url: Url
  };
  if(type == 'formPost' || type == 'formUpdate'){
    Data ? settings.data = Data : '';
  } else{
    Data ? settings.data = JSON.stringify(Data) : '';
  }
  var authHeader = {
    "Authorization": "Token " + localStorage.wutkn
  };
  var postHeaders = {
    "Authorization": "Token " + localStorage.wutkn,
    "content-type" : "application/json"
  };
  switch (type) {
      case 'list':
          settings.type = 'GET';
          settings.headers = localStorage.wutkn ? authHeader : {};
          break;
      case 'listWithToken':
          settings.type = 'GET';
          settings.headers = authHeader;
          break;
      case 'postList':
          settings.type = 'POST';
          settings.headers = localStorage.wutkn ? postHeaders : {"content-type" : "application/json"};
          break;
      case 'auth':
          settings.type = 'POST';
          settings.headers = {"content-type" : "application/json"};
          break;
      case 'post':
          settings.type = 'POST';
          settings.headers = postHeaders;
          break;
      case 'formPost':
          settings.type = 'POST';
          settings.contentType = false;
          settings.processData = false;
          settings.crossDomain = true;
          settings.headers = authHeader;
          break;
      case 'update':
          settings.type = 'PUT';
          settings.headers = postHeaders;
          break;
      case 'formUpdate':
          settings.type = 'PUT';
          settings.contentType = false;
          settings.processData = false;
          settings.crossDomain = true;
          settings.headers = authHeader;
          break;
      case 'delete':
          settings.type = 'DELETE';
          settings.headers = postHeaders;
          break;
      case 'patch':
          settings.type = 'PATCH';
          settings.headers = postHeaders;
          break;
  }
  return $.ajax(settings);
}

var date = x => ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"][new Date(x).getMonth()] + " " + new Date(x).getDate() + ", " + new Date(x).getFullYear();

//function for star append
function starz(star, id, cl) {
  $("." + cl + id).empty();
  if (star) {
    var whole = Math.floor(star);
    var high = 5 - Math.ceil(star);
    var dec = (star * 10) % 10;
    for (var i = 0; i < whole; i++) {
      $("." + cl + id).append(`<li><a class="fa fa-star"></a></li>`);
    }
    if (dec > 4) {
      $("." + cl + id).append(`<li><a class="fa fa-star-half-o"></a></li>`);
    } else if (dec < 5 && dec != 0) {
      $("." + cl + id).append(`<li><a class="fa fa-star-o"></a></li>`);
    }
    for (var i = 0; i < high; i++) {
      $("." + cl + id).append(`<li><a class="fa fa-star-o"></a></li>`);
    }
  } else {
    for (var i = 0; i < 5; i++) {
      $("." + cl + id).append(`<li><a class="fa fa-star-o"></a></li>`);
    }
  }
}